import { CommandLineParser } from "./lib/commands/command-line-parser.mjs";
import { CommandExecutor } from "./lib/commands/command-executor.mjs";
import { MangaDataEditor } from "./lib/helpers/manga-data-editor.mjs";

import {
  ScrapMangaCommand,
  AddMangaCommand,
  ListMangaCommand,
  RemoveMangaCommand,
  GetMangaCommand,
} from "./lib/commands/index.mjs";

//TODO: implement a processing of a chapter direct link
(async () => {
  try {
    await MangaDataEditor.createDataFileIfNotExists();
    const commandLineParser = new CommandLineParser(process.argv);
    const { command, options } = commandLineParser.parse();
    const mangaDataEditor = new MangaDataEditor();

    const commandToExecutorMap = {
      scrap: new ScrapMangaCommand(mangaDataEditor, options),
      list: new ListMangaCommand(mangaDataEditor),
      get: new GetMangaCommand(mangaDataEditor, options),
      add: new AddMangaCommand(mangaDataEditor, options),
      remove: new RemoveMangaCommand(mangaDataEditor, options),
    };

    //TODO: Possibly it's redundant
    const commandExecutor = new CommandExecutor(commandToExecutorMap);
    await commandExecutor.executeCommand(command);
  } catch (error) {
    console.log(error.message);
  }
})();
