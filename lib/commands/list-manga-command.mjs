import { Command } from "./command-interface.mjs";

export class ListMangaCommand extends Command {
  constructor(mangaDataEditor) {
    super();
    this.mangaDataEditor = mangaDataEditor;
  }

  async execute() {
    await this.mangaDataEditor.list();
  }
}
