//TODO: Possibly it's useless
export class CommandExecutor {
  constructor(commandToExecutorMap) {
    this.commandToExecutorMap = commandToExecutorMap;
  }

  async executeCommand(commandName){
    const command = this.commandToExecutorMap[commandName];
    if(typeof command.execute === "function"){
      await command.execute();
    }
  }
}
