import { ScrapMangaCommand } from "./scrap-manga-command.mjs";
import { AddMangaCommand } from "./add-manga-command.mjs";
import { ListMangaCommand } from "./list-manga-command.mjs";
import { RemoveMangaCommand } from "./remove-manga-command.mjs";
import { GetMangaCommand } from "./get-manga-command.mjs";

export {
  ScrapMangaCommand,
  AddMangaCommand,
  ListMangaCommand,
  RemoveMangaCommand,
  GetMangaCommand,
};
