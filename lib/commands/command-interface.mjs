//TODO: Probably such kind of "abstract class" or "interface" makes no sense in JS
export class Command {
  constructor() {
    if(this.constructor === Command){
      throw new Error('Object of interface Command cannot be created')
    }
  }
  async execute(){}
}