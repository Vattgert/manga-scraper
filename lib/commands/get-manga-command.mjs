import { Command } from "./command-interface.mjs";

export class GetMangaCommand extends Command {
  constructor(mangaDataEditor, options) {
    super();
    this.mangaDataEditor = mangaDataEditor;
    this.options = options;
  }

  async execute() {
    const codename = this.options.codename;
    const { link, path, title } = await this.mangaDataEditor.get(codename);
    this.mangaDataEditor.show({ codename, link, path, title });
  }
}
