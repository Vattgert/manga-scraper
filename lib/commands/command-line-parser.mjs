import minimist from "minimist";
import { commands } from "./commands.mjs";
import {BadCommandError} from "../errors/BadCommandError.mjs";

export class CommandLineParser {
  //TODO: throw error if bad params
  constructor(commandLineArgs) {
    commandLineArgs = commandLineArgs.slice(2);
    this.argv = minimist(commandLineArgs);
    const paramsArray = this.argv._;
    if (Array.isArray(paramsArray) && paramsArray.length >= 1) {
      const command = this.argv._[0];
      this.command = command;
      this.commandOptions = commands[command];
    }
  }

  parse() {
    if(this.commandOptions === undefined){
      throw new BadCommandError(this.command);
    }
    const options = this.#parseOptions(this.commandOptions, this.argv);
    return {
      command: this.command,
      options,
    };
  }

  #parseOptions(command) {
    function transformOrReturn(value, transformFunc) {
      if (typeof transformFunc === "function") {
        return transformFunc(value);
      } else {
        return value;
      }
    }

    const result = {};
    const { options } = command;
    for (const option in options) {
      const optionSettings = options[option];
      const { withoutOptionName, place, transform } = optionSettings;
      let optionValue = undefined;
      if (withoutOptionName && place > 0) {
        optionValue = this.argv._[place] || this.argv[option];
      } else {
        optionValue = this.argv[option];
      }
      if (optionValue !== undefined) {
        result[option] = transformOrReturn(optionValue, transform);
      }
    }
    return result;
  }
}
