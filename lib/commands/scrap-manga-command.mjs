import { Command } from "./command-interface.mjs";
import { Scraper } from "../scraper.mjs";
import { setupBrowser } from "../helpers/browser.mjs";
import { getStrategyByDomainName } from "../strategy/strategy-manager.mjs";

//TODO: Class has dependencies which make it hard-testable. Make it easier to test
export class ScrapMangaCommand extends Command {
  constructor(mangaEditor, commandLineOptions) {
    super();
    this.commandLineOptions = commandLineOptions;
    this.mangaEditor = mangaEditor;
  }

  async execute() {
    const mangaData = await this.mangaEditor.get(
      this.commandLineOptions.mangaName
    );
    const scrappingOptions = Object.assign(
      {},
      this.commandLineOptions,
      mangaData
    );
    const { browser, page } = await setupBrowser();
    const MangaStrategy = getStrategyByDomainName(scrappingOptions.link);
    const scraper = new Scraper(page, MangaStrategy, scrappingOptions);
    await scraper.scrap();
    await browser.close();
  }
}
