export const commands = {
  scrap: {
    options: {
      mangaName: { required: true, withoutOptionName: true, place: 1 },
      getLast: {
        required: false,
        transform: (paramValue) => paramValue === "true",
      },
      toSeparateFolder: {
        required: false,
        transform: (paramValue) => paramValue === "true",
      },
      chapterDirectLink: { required: false },
    },
  },
  add: {
    options: {
      codename: { required: true, withoutOptionName: true, place: 1 },
      link: { required: true, withoutOptionName: true, place: 2 },
      path: { required: true, withoutOptionName: true, place: 3 },
      title: { required: false, withoutOptionName: true, place: 4 },
    },
  },
  edit: {
    options: ["codename", "link", "path", "title"],
  },
  remove: {
    options: {
      codename: { required: true, withoutOptionName: true, place: 1 }
    },
  },
  list: {},
  get: {
    options: {
      codename: { required: true, withoutOptionName: true, place: 1 }
    },
  }
};
