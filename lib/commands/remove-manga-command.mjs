import { Command } from "./command-interface.mjs";

export class RemoveMangaCommand extends Command {
  constructor(mangaDataEditor, options) {
    super();
    this.mangaDataEditor = mangaDataEditor;
    this.options = options;
  }

  async execute() {
    await this.mangaDataEditor.remove(this.options.codename);
  }
}
