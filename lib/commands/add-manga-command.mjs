import { Command } from "./command-interface.mjs";

//TODO make this testable with low coupling
export class AddMangaCommand extends Command {
  constructor(mangaDataEditor, options) {
    super();
    this.mangaDataEditor = mangaDataEditor;
    this.options = options;
  }

  async execute(){
    this.mangaDataEditor.add(this.options);
  }
}
