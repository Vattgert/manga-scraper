export class Chapter {
  constructor({ chapterNumber, pages }) {
    this.chapterNumber = chapterNumber;
    this.pages = pages;
  }
}
