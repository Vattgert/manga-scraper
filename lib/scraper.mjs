import { MangaFileStorage } from "./helpers/manga-file-storage.mjs";

export class Scraper {
  constructor(page, MangaStrategy, options) {
    this.page = page;
    this.strategy = new MangaStrategy(page);
    this.mangaFileStorage = new MangaFileStorage();
    this.options = options;
  }

  async getChapterAndSave(link, { path, toSeparateFolder }) {
    const chapter = await this.strategy.getChapter(link);
    await this.mangaFileStorage.saveChapterPages(chapter, {
      path,
      toSeparateFolder,
    });
  }

  async scrap() {
    const { path, link, chapterDirectLink, toSeparateFolder, getLast } =
      this.options;
    this.mangaFileStorage.createFolderIfDoesNotExist(path);
    if (chapterDirectLink) {
      await this.getChapterAndSave(chapterDirectLink, {
        path,
        toSeparateFolder,
      });
    } else {
      await this.page.goto(link);
      const chaptersLinks = await this.strategy.getMangaChaptersLinks(getLast);
      for (let link of chaptersLinks) {
        await this.getChapterAndSave(link, {
          path,
          toSeparateFolder,
        });
      }
    }
  }
}
