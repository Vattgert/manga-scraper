import { MangaclashStrategy } from "./mangaclash-strategy.mjs";
import { ReadmangaStrategy } from "./readmanga-strategy.mjs";
import { MangalibStrategy } from "./mangalib-strategy.mjs";
import { MangaKakalotStrategy } from "./manga-kakalot-strategy.mjs";

function getMangaStrategy(mangaSourceWebsite) {
  const mangaSourceToStrategyMap = {
    mangaclash: MangaclashStrategy,
    readmanga: ReadmangaStrategy,
    mangalib: MangalibStrategy,
    mangakakalot: MangaKakalotStrategy,
  };

  return mangaSourceToStrategyMap[mangaSourceWebsite];
}

function getWebsiteName(hostName) {
  const splittedHostName = hostName.split(".");
  splittedHostName.splice(-1, 1);
  let websiteName = "";
  if (splittedHostName.length === 1) {
    websiteName = splittedHostName[0];
  }
  return websiteName;
}

export function getStrategyByDomainName(link) {
  const mangaUrl = new URL(link);
  const hostName = mangaUrl.hostname;
  const websiteName = getWebsiteName(hostName);
  return getMangaStrategy(websiteName);
}
