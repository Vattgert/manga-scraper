import { MangaStrategy } from "./strategy.mjs";

export class ReadmangaStrategy extends MangaStrategy {
  constructor(page) {
    super(page);
  }

  parseChapterNumber(link) {
    console.log(link);
    const mangaChapterNumberRegExResult = link.match(
      /[0-9]{1,5}([,.][0-9]{1,2})?$/g
    );
    const mangaChapterVolumeRegExResult = link.match(/(?<=vol)([0-9]{1,5})/g);

    const mangaChapterNumberString = mangaChapterNumberRegExResult[0];
    const mangaVolumeNumberString = mangaChapterVolumeRegExResult[0];

    return `${mangaVolumeNumberString}.${mangaChapterNumberString}`;
  }

  async getChapterImagesLinks() {
    return this.page.evaluate(async () => {
      let chapterImageElement = null;
      const imagesLinks = [];
      while (true) {
        chapterImageElement = document.querySelector(
          "#fotocontext > #mangaPicture"
        );
        if (chapterImageElement !== null) {
          const imageLink = chapterImageElement.getAttribute("src");
          if (
            imagesLinks.length !== 0 &&
            imageLink === imagesLinks[imagesLinks.length - 1]
          ) {
            break;
          }
          imagesLinks.push(imageLink);
          await chapterImageElement.click();
        }
      }
      return [...new Set(imagesLinks)];
    });
  }

  async getMangaChaptersLinks(getLast) {
    const mangaChaptersLinks = await this.page.evaluate((getLast) => {
      const chaptersListElement = document.querySelector(".table");
      let chaptersListItems = chaptersListElement.querySelectorAll("td a");
      let chapterLinks = [...chaptersListItems].map((link) => {
        return `https://readmanga.io/${link.getAttribute("href")}`;
      });
      if (getLast) {
        const oneItemArray = [];
        oneItemArray.push(chapterLinks[0]);
        chapterLinks = oneItemArray;
      }
      return chapterLinks;
    }, getLast);
    return mangaChaptersLinks || [];
  }
}
