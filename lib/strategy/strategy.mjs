import { Chapter } from "../models/chapter.mjs";

export class MangaStrategy {
  constructor(page) {
    this.page = page;
    if (this.constructor === MangaStrategy) {
      throw new Error(
        `${this.constructor} abstract class can't be instantiated.`
      );
    }
  }

  parseChapterNumber(chapterLink) {
    throw new Error("Not implemented!");
  }

  getChapterImagesLinks() {
    throw new Error("Not implemented!");
  }

  getMangaChaptersLinks(getLast) {
    throw new Error("Not implemented!");
  }

  async getChapter(link) {
    const chapterNumber = this.parseChapterNumber(link);
    console.log(`Getting Chapter ${chapterNumber}`);
    await this.page.goto(link);
    const chapterPages = await this.getChapterImagesLinks(this.page);
    const pages = await this.loadChapterPages(this.page, {
      chapterPages,
      chapterNumber,
    });
    return new Chapter({ chapterNumber, pages });
  }

  async #loadChapterImage(chapterImageLink) {
    const imageViewSource = await this.page.goto(chapterImageLink);
    return imageViewSource.buffer();
  }

  async loadChapterPages(imagePage, { chapterPages, chapterNumber }) {
    const pages = [];
    for (let pageIndex = 0; pageIndex < chapterPages.length; pageIndex++) {
      const pageSrc = chapterPages[pageIndex];
      const pageNumber = pageIndex + 1;
      console.log(`Page visiting: ${pageSrc}.`);
      try {
        const page = await this.#loadChapterImage(pageSrc);
        pages.push({ pageNumber, page });
      } catch (error) {
        console.log(
          `Cannot get chapter ${chapterNumber} on page ${pageNumber}.`
        );
      }
    }
    return pages;
  }
}
