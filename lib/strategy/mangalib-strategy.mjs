import {MangaStrategy} from "./strategy.mjs";

export class MangalibStrategy extends MangaStrategy {
  constructor(page) {
    super(page);
  }

  parseChapterNumber(link) {
    const mangaChapterCounterRegExResult = link.match(
      /(?<=c)[0-9]{1,5}([,.][0-9]{1,2})?/g
    );
    return mangaChapterCounterRegExResult[0];
  }

  async getChapterImagesLinks() {
    return this.page.evaluate(async () => {
      let chapterImageElements = document.querySelectorAll(
        ".reader-view .reader-view__wrap > img"
      );
      const chapterImageElementsArray = [...chapterImageElements];
      const maxChaptersNumber = chapterImageElementsArray.length;
      let i = 0;
      while (i < maxChaptersNumber) {
        const firstElement = chapterImageElementsArray[i];
        await firstElement.click();
      }
      chapterImageElements = document.querySelectorAll(
        ".reader-view .reader-view__wrap > img"
      );
      return [...chapterImageElements].map((element) =>
        element.getAttribute("src")
      );
    });
  }
}
