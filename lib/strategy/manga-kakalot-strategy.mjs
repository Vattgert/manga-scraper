import { MangaStrategy } from "./strategy.mjs";

export class MangaKakalotStrategy extends MangaStrategy {
  constructor(page) {
    super(page);
  }

  parseChapterNumber(chapterLink) {
    const mangaChapterNumberRegExResult = chapterLink.match(
      /(?<=chapter_)[0-9]{1,5}([,.-][0-9]{1,2})?/g
    );
    return mangaChapterNumberRegExResult[0];
  }

  getChapterImagesLinks() {
    return this.page.evaluate(() => {
      const chapterImagesElements = document.querySelectorAll(
        ".container-chapter-reader > img"
      );
      return [...chapterImagesElements].map((image) => {
        return image.getAttribute("src") != null
          ? image.getAttribute("src")
          : image.getAttribute("data-src");
      });
    });
  }

  async getMangaChaptersLinks(getLast) {
    const mangaChaptersLinks = await this.page.evaluate((getLast) => {
      const chaptersListElement = document.querySelector(".chapter-list");
      let chaptersListItems = chaptersListElement.querySelectorAll(".row a");
      let chapterLinks = [...chaptersListItems].map((link) => {
        return link.getAttribute("href");
      });
      if (getLast) {
        const oneItemArray = [];
        oneItemArray.push(chapterLinks[0]);
        chapterLinks = oneItemArray;
      }
      return chapterLinks;
    }, getLast);
    return mangaChaptersLinks || [];
  }
}
