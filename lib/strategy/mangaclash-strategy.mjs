import {MangaStrategy} from "./strategy.mjs";

export class MangaclashStrategy extends MangaStrategy {
  constructor(page) {
    super(page);
  }

  parseChapterNumber(link) {
    const mangaChapterNumberRegExResult = link.match(/[0-9]{1,5}([,.-][0-9]{1,2})?/g);
    let mangaChapterNumberString = mangaChapterNumberRegExResult[0];
    mangaChapterNumberString = mangaChapterNumberString.replace(/[,-]/, '.');
    return mangaChapterNumberString;
  }

  async getChapterImagesLinks(page) {
    return this.page.evaluate(() => {
      const chapterImagesElements =
        document.querySelectorAll(".page-break > img");
      return [...chapterImagesElements].map((image) => {
        return image.getAttribute("src") != null
            ? image.getAttribute("src")
            : image.getAttribute("data-src");
      });
    });
  }

  async getMangaChaptersLinks(getLast) {
    const mangaChaptersLinks = await this.page.evaluate((getLast) => {
      const chaptersListElement = document.querySelector("ul.version-chap");
      let chaptersListItems = chaptersListElement.querySelectorAll(
        "li.wp-manga-chapter > a"
      );
      let chapterLinks = [...chaptersListItems].map((link) => {
        return link.getAttribute("href");
      });
      if (getLast) {
        const oneItemArray = [];
        oneItemArray.push(chapterLinks[0]);
        chapterLinks = oneItemArray;
      }
      return chapterLinks;
    }, getLast);
    return mangaChaptersLinks || [];
  }
}
