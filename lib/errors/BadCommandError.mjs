export class BadCommandError extends Error {
  constructor(commandName) {
    const message = `Error: undefined command "${commandName}".`;
    super(message);
    this.command = commandName;
  }
}