import { readFile, writeFile, mkdir, access } from "fs/promises";
import { homedir } from "os";
import { join } from "path";

//TODO: Make it more testable
export class MangaDataEditor {
  constructor() {
    this.DEFAULT_MANGA_DATA_PATH = join(
      homedir(),
      ".manga-scraper",
      "mangaData.json"
    );
  }

  static async createDataFileIfNotExists() {
    const dataFileName = "mangaData.json";
    const folderName = ".manga-scraper";
    const folderPath = join(homedir(), folderName);
    const filePath = join(folderPath, dataFileName);
    try {
      await access(folderPath);
    } catch (error) {
      console.log(`Created ${filePath} file to store manga data.`);
      await mkdir(folderPath);
      const jsonInitialValue = JSON.stringify({});
      await writeFile(filePath, jsonInitialValue);
    }
  }

  async #getMangaDataFile() {
    return readFile(this.DEFAULT_MANGA_DATA_PATH);
  }

  async #getMangaData() {
    const mangaDataBuffer = await this.#getMangaDataFile();
    return JSON.parse(mangaDataBuffer.toString());
  }

  show({ codename, link, path, title = "is not provided" }) {
    console.log(`\nCodename: ${codename}`);
    console.log(`Full title: ${title}`);
    console.log(`All chapters: ${link}`);
    console.log(`Folder path: ${path}\n`);
  }

  async list() {
    const mangaData = await this.#getMangaData();
    for (let codename in mangaData) {
      const { link, path, title } = mangaData[codename];
      this.show({ codename, link, path, title });
    }
  }

  async add(options) {
    //TODO: validate options
    try {
      const { codename, ...properties } = options;
      const mangaData = await this.#getMangaData();
      mangaData[codename] = properties;
      const json = JSON.stringify(mangaData);
      await writeFile(this.DEFAULT_MANGA_DATA_PATH, json, {
        encoding: "utf8",
      });
    } catch (error) {
      console.log(error);
      console.log(`Cannot add manga data.`);
    }
  }

  async get(codename) {
    try {
      const mangaData = await this.#getMangaData();
      return mangaData[codename];
    } catch (error) {
      console.log(`Cannot get manga data by "${codename}" codename`);
    }
  }

  async remove(codename) {
    try {
      const mangaData = await this.#getMangaData();
      const deleted = delete mangaData[codename];
      const json = JSON.stringify(mangaData);
      await writeFile(this.DEFAULT_MANGA_DATA_PATH, json, {
        encoding: "utf8",
      });
      return deleted;
    } catch (error) {
      console.log(`Cannot get manga data by "${codename}" codename`);
      return false;
    }
  }
}
