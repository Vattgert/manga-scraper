import path from "path";

export class MangaPathBuilder {
  constructor(mangaPath) {
    this.WINDOWS_DEFAULT_PATH = "C:\\Users\\vattg\\Desktop";
    this.LINUX_DEFAULT_PATH = "/home/manga";
    this.mangaPathParams = Object.assign(
      {},
      {
        mangaPath: mangaPath || this.#getDefaultPlatformPath(),
        chapterFolderPath: "",
        chapterPageFilename: "",
      }
    );
  }

  #getDefaultPlatformPath() {
    switch (process.platform) {
      case "win32":
        return this.WINDOWS_DEFAULT_PATH;
      case "linux":
        return this.LINUX_DEFAULT_PATH;
      default:
        throw new Error(
          `Paths for ${process.platform} platform are not supported yet.`
        );
    }
  }

  setChapterFolderPath(chapterNumber) {
    if (typeof chapterNumber === "number" && chapterNumber >= 0) {
      this.mangaPathParams.chapterFolderPath = `Chapter ${chapterNumber}`;
    }
    return this;
  }

  setChapterPageFilename(chapterNumber, pageNumber) {
    this.mangaPathParams.chapterPageFilename = `chapter-${chapterNumber}-page-${pageNumber}.png`;
    return this;
  }

  create() {
    const { mangaPath, chapterFolderPath, chapterPageFilename } =
      this.mangaPathParams;
    return path.join(mangaPath, chapterFolderPath, chapterPageFilename);
  }
}
