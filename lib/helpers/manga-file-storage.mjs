import { writeFile as writeFileFs, existsSync, mkdirSync } from "fs";
import { mkdir } from "fs/promises";
import { promisify } from "util";
import { MangaPathBuilder } from "./manga-path-builder.mjs";
const writeFile = promisify(writeFileFs);

export class MangaFileStorage {
  createFolderIfDoesNotExist(path) {
    if (!existsSync(path)) {
      mkdirSync(path);
    }
  }

  async saveMangaPage(filePath, buffer) {
    console.log(`Page saving to ${filePath}`);
    writeFile(filePath, buffer);
  }

  async saveChapterPages(chapter, { path, toSeparateFolder }) {
    let folderPath = path;
    if (toSeparateFolder === true) {
      folderPath = new MangaPathBuilder(path)
        .setChapterFolderPath(chapter.chapterNumber)
        .create();
      await this.createSeparateFolderForChapter(folderPath);
    }
    for (let { page, pageNumber } of chapter.pages) {
      let mangaPath = new MangaPathBuilder(folderPath)
        .setChapterPageFilename(chapter.chapterNumber, pageNumber)
        .create();
      await this.saveMangaPage(mangaPath, page);
    }
  }

  async createSeparateFolderForChapter(path) {
    try {
      await mkdir(path);
    } catch (error) {
      console.log(`Folder ${path} cannot be created.`);
    }
  }
}
