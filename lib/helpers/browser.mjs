import puppeteer from "puppeteer";

export async function setupBrowser() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setDefaultNavigationTimeout(0);
  //Provides an ability to load large files
  const client = await page.target().createCDPSession();
  await client.send("Network.enable", {
    maxResourceBufferSize: 1024 * 1204 * 100,
    maxTotalBufferSize: 1024 * 1204 * 200,
  });
  return { browser, page };
}